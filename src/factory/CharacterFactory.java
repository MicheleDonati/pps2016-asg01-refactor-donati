package factory;

import characters.BasicGameCharacter;
import strategies.ContactCharacterStrategy;

/**
 * Created by Michele on 15/03/2017.
 */
public interface CharacterFactory {
    BasicGameCharacter createMario(int x, int y);
    BasicGameCharacter createMushroom(int x, int y);
    BasicGameCharacter createTurtle(int x, int y);
}
