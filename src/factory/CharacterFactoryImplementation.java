package factory;

import characters.BasicGameCharacter;
import characters.Mario;
import characters.Mushroom;
import characters.Turtle;
import strategies.ContactCharacterStrategy;

/**
 * Created by Michele on 15/03/2017.
 */
public class CharacterFactoryImplementation implements CharacterFactory {
    @Override
    public BasicGameCharacter createMario(int x, int y) {
        return new Mario(x, y);
    }

    @Override
    public BasicGameCharacter createMushroom(int x, int y) {
        return new Mushroom(x, y);
    }

    @Override
    public BasicGameCharacter createTurtle(int x, int y) {
        return new Turtle(x, y);
    }
}
