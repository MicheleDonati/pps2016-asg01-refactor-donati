package factory;

import objects.GameObject;

/**
 * Created by Michele on 15/03/2017.
 */
public interface ObjectFactory {

    GameObject createBlock(int x, int y);
    GameObject createPiece(int x, int y);
    GameObject createTunnel(int x, int y);
}
