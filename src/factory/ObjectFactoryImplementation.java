package factory;

import objects.*;

/**
 * Created by Michele on 15/03/2017.
 */
public class ObjectFactoryImplementation implements ObjectFactory {
    @Override
    public GameObject createBlock(int x, int y) {
        return new Block(x, y);
    }

    @Override
    public GameObject createPiece(int x, int y) {
        return new Piece(x, y);
    }

    @Override
    public GameObject createTunnel(int x, int y) {
        return new Tunnel(x, y);
    }
}
