package objects;

import utils.Resources;
import utils.Utils;

import java.awt.Image;

public class Piece extends GameObject implements Runnable {

    private int counter;

    public Piece(int x, int y) {
        super(x, y, Resources.PIECE_WIDTH, Resources.PIECE_HEIGHT);
        super.imageObject = Utils.getImage(Resources.IMG_PIECE1);
    }

    public Image imageOnMovement() {
        return Utils.getImage(++this.counter % Resources.FLIP_FREQUENCY == 0 ? Resources.IMG_PIECE1 : Resources.IMG_PIECE2);
    }

    @Override
    public void run() {
        while (true) {
            this.imageOnMovement();
            try {
                Thread.sleep(Resources.PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

}
