package objects;

import java.awt.Image;

import game.Main;

public abstract class GameObject {

    private int width,
                height,
                coordinateX,
                coordinateY;

    protected Image imageObject;

    public GameObject(int x, int y, int width, int height) {
        this.coordinateX = x;
        this.coordinateY = y;
        this.width = width;
        this.height = height;
    }

    public abstract Image imageOnMovement();

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getCoordinateX() {
        return coordinateX;
    }

    public int getCoordinateY() {
        return coordinateY;
    }

    public Image getImageObject() {
        return imageObject;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setCoordinateX(int x) {
        this.coordinateX = x;
    }

    public void setCoordinateY(int y) {
        this.coordinateY = y;
    }

    public void move() {
        if (Main.scene.getxPosition() >= 0) {
            this.coordinateX = this.coordinateX - Main.scene.getMovement();
        }
    }

}
