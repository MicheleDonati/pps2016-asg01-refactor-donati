package objects;

import utils.Resources;
import utils.Utils;

import java.awt.*;

public class Tunnel extends GameObject {

    public Tunnel(int x, int y) {
        super(x, y, Resources.TUNNEL_WIDTH, Resources.TUNNEL_HEIGHT);
        super.imageObject = Utils.getImage(Resources.IMG_TUNNEL);
    }

    @Override
    public Image imageOnMovement() {
        return null;
    }
}
