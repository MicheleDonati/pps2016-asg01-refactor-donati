package objects;

import utils.Resources;
import utils.Utils;

import java.awt.*;

public class Block extends GameObject {

    public Block(int x, int y) {
        super(x, y, Resources.BLOCK_WIDTH, Resources.BLOCK_HEIGHT);
        super.imageObject = Utils.getImage(Resources.IMG_BLOCK);
    }

    @Override
    public Image imageOnMovement() {
        return null;
    }
}
