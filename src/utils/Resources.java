package utils;

/**
 * @author Roberto Casadei
 */

public class Resources {

    /***** BasicGameCharacter specific constants *****/
    public static final int PROXIMITY_MARGIN = 10,
                            STARTER_COUNTER = 0;

    /***** Mario specific constants *****/
    public static final int MARIO_OFFSET_Y_INITIAL = 243,
                            FLOOR_OFFSET_Y_INITIAL = 293,
                            WIDTH = 28,
                            HEIGHT = 50,
                            JUMPING_LIMIT = 42,
                            EXTENSION_OF_JUMP = 0;

    /***** Mushroom specific constants *****/
    public static final int MUSHROOM_WIDTH = 27,
                            MUSHROOM_HEIGHT = 30,
                            MUSHROOM_OFFSET_COORDINATE_X = 1;

    /***** Turtle specific constants *****/
    public static final int TURTLE_WIDTH = 43,
                            TURTLE_HEIGHT = 50;

    /***** Keyboard specific constants *****/
    public final static int LEFT = 1,
                            RIGHT = -1,
                            DONT_MOVE = 0;

    /***** Main specific constants *****/
    public static final int WINDOW_WIDTH = 700;
    public static final int WINDOW_HEIGHT = 360;
    public static final String WINDOW_TITLE = "Super Mario";

    /***** Platform specific constants *****/
    public static final int MARIO_FREQUENCY = 25,
                            MUSHROOM_FREQUENCY = 45,
                            TURTLE_FREQUENCY = 45,
                            MUSHROOM_DEAD_OFFSET_Y = 20,
                            TURTLE_DEAD_OFFSET_Y = 30,
                            FLAG_X_POS = 4650,
                            CASTLE_X_POS = 4850,
                            FLAG_Y_POS = 115,
                            CASTLE_Y_POS = 145,
                            POSITION_X_BACKGROUND1 = -50,
                            POSITION_X_BACKGROUND2 = 750,
                            MOVEMENT = 0,
                            X_POSITION = -1,
                            FLOOR_OFFSET_Y = 293,
                            HEIGHT_LIMIT = 0,
                            GAME_OVER_OFFSET_X = 275,
                            GAME_OVER_OFFSET_Y = 100;

    /***** Scene specific constants *****/
    public static final int BACKGROUND_OFFSET_X = -50,
                            BACKGROUND_OFFSET_Y = 0,
                            MARIO_OFFSET_X = 300,
                            MARIO_OFFSET_Y = 245;

    /***** Block specific constants *****/
    public static final int BLOCK_WIDTH = 30,
                            BLOCK_HEIGHT = 30;

    /***** Piece specific constants *****/
    public static final int PIECE_WIDTH = 30,
                            PIECE_HEIGHT = 30,
                            PAUSE = 10,
                            FLIP_FREQUENCY = 100;

    /***** Tunnel specific constants *****/
    public static final int TUNNEL_WIDTH = 43,
                            TUNNEL_HEIGHT = 65;


    public static final String IMG_BASE = "/resources/imgs/";
    public static final String AUDIO_BASE = "/resources/audio/";
    public static final String IMG_EXT = ".png";

    public static final String IMGP_STATUS_NORMAL = "";
    public static final String IMGP_STATUS_ACTIVE = "A";
    public static final String IMGP_STATUS_DEAD = "E";
    public static final String IMGP_STATUS_IDLE = "F";
    public static final String IMGP_STATUS_SUPER = "S";

    public static final String IMGP_DIRECTION_SX = "G";
    public static final String IMGP_DIRECTION_DX = "D";

    public static final String IMGP_CHARACTER_MUSHROOM = "mushroom";
    public static final String IMGP_CHARACTER_TURTLE = "turtle";
    public static final String IMGP_CHARACTER_MARIO = "mario";

    public static final String IMGP_OBJECT_BLOCK = "block";
    public static final String IMGP_OBJECT_PIECE1 = "piece1";
    public static final String IMGP_OBJECT_PIECE2 = "piece2";
    private static final String IMGP_OBJECT_TUNNEL = "tunello";

    public static final String IMG_MARIO_DEFAULT = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_SUPER_SX = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_SUPER + IMGP_DIRECTION_SX + IMG_EXT;
    public static final String IMG_MARIO_SUPER_DX = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_SUPER + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_ACTIVE_SX = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_ACTIVE + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_ACTIVE_DX = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_ACTIVE + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_SX = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_NORMAL + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_DX = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_NORMAL + IMGP_DIRECTION_DX + IMG_EXT;

    public static final String IMG_MUSHROOM_DX = IMG_BASE + IMGP_CHARACTER_MUSHROOM + IMGP_STATUS_NORMAL + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MUSHROOM_SX = IMG_BASE + IMGP_CHARACTER_MUSHROOM + IMGP_STATUS_NORMAL + IMGP_DIRECTION_SX + IMG_EXT;
    public static final String IMG_MUSHROOM_DEAD_DX = IMG_BASE + IMGP_CHARACTER_MUSHROOM + IMGP_STATUS_DEAD + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MUSHROOM_DEAD_SX = IMG_BASE + IMGP_CHARACTER_MUSHROOM + IMGP_STATUS_DEAD + IMGP_DIRECTION_SX + IMG_EXT;
    public static final String IMG_MUSHROOM_DEFAULT = Resources.IMG_BASE + Resources.IMGP_CHARACTER_MUSHROOM + Resources.IMGP_STATUS_ACTIVE + IMGP_DIRECTION_DX + IMG_EXT;

    public static final String IMG_TURTLE_IDLE = IMG_BASE + IMGP_CHARACTER_TURTLE + IMGP_STATUS_IDLE + IMG_EXT;
    public static final String IMG_TURTLE_DEAD = IMG_TURTLE_IDLE;

    public static final String IMG_BLOCK = IMG_BASE + IMGP_OBJECT_BLOCK + IMG_EXT;

    public static final String IMG_PIECE1 = IMG_BASE + IMGP_OBJECT_PIECE1 + IMG_EXT;
    public static final String IMG_PIECE2 = IMG_BASE + IMGP_OBJECT_PIECE2 + IMG_EXT;
    public static final String IMG_TUNNEL = IMG_BASE + IMGP_OBJECT_TUNNEL + IMG_EXT;

    public static final String IMG_BACKGROUND = IMG_BASE + "background" + IMG_EXT;
    public static final String IMG_CASTLE = IMG_BASE + "castelloIni" + IMG_EXT;
    public static final String START_ICON = IMG_BASE + "start" + IMG_EXT;
    public static final String IMG_CASTLE_FINAL = IMG_BASE + "castelloF" + IMG_EXT;
    public static final String IMG_FLAG = IMG_BASE + "bandiera" + IMG_EXT;

    public static final String AUDIO_MONEY = AUDIO_BASE + "money.wav";

    public static final String IMG_GAME_OVER = IMG_BASE + "gameover" + IMG_EXT;
}
