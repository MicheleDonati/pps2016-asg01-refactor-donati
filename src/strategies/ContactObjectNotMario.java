package strategies;

import characters.BasicGameCharacter;
import objects.GameObject;

/**
 * Created by Michele on 18/03/2017.
 */
public class ContactObjectNotMario implements ContactObjectStrategy {
    @Override
    public void contact(BasicGameCharacter character, GameObject object) {
        if (character.hitAhead(object) && character.isToRight()) {
            character.setToRight(false);
            character.setOffsetX(-1);
        } else if (character.hitBack(object) && !character.isToRight()) {
            character.setToRight(true);
            character.setOffsetX(-1);
        }
    }
}
