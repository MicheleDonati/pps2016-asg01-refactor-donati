package strategies;

import characters.BasicGameCharacter;
import game.Main;
import objects.GameObject;
import utils.Resources;

/**
 * Created by Michele on 18/03/2017.
 */
public class ContactObjectMario implements ContactObjectStrategy {
    @Override
    public void contact(BasicGameCharacter character, GameObject object) {
        if (character.hitAhead(object) && character.isToRight() || character.hitBack(object) && !character.isToRight()) {
            Main.scene.setMovement(0);
            character.setMoving(false);
        }

        if (character.hitBelow(object) && character.isJumping()) {
            Main.scene.setFloorOffsetY(object.getCoordinateY());
        } else if (!character.hitBelow(object)) {
            Main.scene.setFloorOffsetY(Resources.FLOOR_OFFSET_Y_INITIAL);
            if (!character.isJumping()) {
                character.setCoordinateY(Resources.MARIO_OFFSET_Y_INITIAL);
            }

            if (character.hitAbove(object)) {
                Main.scene.setHeightLimit(object.getCoordinateY() + object.getHeight()); // the new sky goes below the object
            } else if (!character.hitAbove(object) && !character.isJumping()) {
                Main.scene.setHeightLimit(0); // initial sky
            }
        }
    }
}
