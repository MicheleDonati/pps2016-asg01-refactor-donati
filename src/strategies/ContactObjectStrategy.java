package strategies;

import characters.BasicGameCharacter;
import objects.GameObject;

/**
 * Created by Michele on 17/03/2017.
 */
public interface ContactObjectStrategy {
    void contact(BasicGameCharacter character, GameObject object);
}
