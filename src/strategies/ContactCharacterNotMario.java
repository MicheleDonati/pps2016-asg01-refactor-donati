package strategies;

import characters.BasicGameCharacter;

/**
 * Created by Michele on 17/03/2017.
 */
public class ContactCharacterNotMario implements ContactCharacterStrategy {
    @Override
    public void contact(BasicGameCharacter notMario, BasicGameCharacter character) {
        if (notMario.hitAhead(character) && notMario.isToRight()) {
            notMario.setToRight(false);
            notMario.setOffsetX(-1);
        } else if (notMario.hitBack(character) && !notMario.isToRight()) {
            notMario.setToRight(true);
            notMario.setOffsetX(-1);
        }
    }
}
