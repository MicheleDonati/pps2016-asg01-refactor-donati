package strategies;

import characters.BasicGameCharacter;

/**
 * Created by Michele on 17/03/2017.
 */
public interface ContactCharacterStrategy  {

    void contact(BasicGameCharacter characterItself, BasicGameCharacter character);
}
