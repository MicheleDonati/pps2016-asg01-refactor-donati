package strategies;

import characters.BasicGameCharacter;
import utils.Resources;
import utils.Utils;

import java.awt.*;

/**
 * Created by Michele on 17/03/2017.
 */
public class ContactCharacterMario implements ContactCharacterStrategy {

    @Override
    public void contact(BasicGameCharacter mario, BasicGameCharacter character) {
        if (mario.hitAhead(character) || mario.hitBack(character)) {
            if (character.isAlive()) {
                mario.setMoving(false);
                mario.setAlive(false);
            } else mario.setAlive(true);
        } else if (mario.hitBelow(character)) {
            character.setMoving(false);
            character.setAlive(false);
        }
    }
}
