package game;

import utils.Resources;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    @Override
    public void keyPressed(KeyEvent e) {

        if (Main.scene.mario.isAlive() == true) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // per non fare muovere il castello e start
                if (Main.scene.getxPosition() == -1) {
                    Main.scene.setxPosition(0);
                    Main.scene.setBackground1PositionX(-50);
                    Main.scene.setBackground2PosX(750);
                }
                Main.scene.mario.setMoving(true);
                Main.scene.mario.setToRight(true);
                Main.scene.setMovement(Resources.LEFT);
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (Main.scene.getxPosition() == 4601) {
                    Main.scene.setxPosition(4600);
                    Main.scene.setBackground1PositionX(-50);
                    Main.scene.setBackground2PosX(750);
                }

                Main.scene.mario.setMoving(true);
                Main.scene.mario.setToRight(false);
                Main.scene.setMovement(Resources.RIGHT);
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                Main.scene.mario.setJumping(true);
                Audio.playSound("/resources/audio/jump.wav");
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Main.scene.mario.setMoving(false);
        Main.scene.setMovement(Resources.DONT_MOVE);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
