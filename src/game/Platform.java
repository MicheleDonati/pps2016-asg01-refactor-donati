package game;

import characters.*;
import factory.*;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.JPanel;
import objects.GameObject;
import strategies.*;
import utils.Resources;
import utils.Utils;

@SuppressWarnings("serial")
public class Platform extends JPanel {

    private Image imageBackground1,
                  imageBackground2,
                  castle,
                  start,
                  imageGameOver;

    private int background1PositionX,
                background2PositionX,
                movement,
                xPosition,
                floorOffsetY,
                heightLimit;

    public BasicGameCharacter mario,
                              mushroom,
                              turtle;

    public GameObject tunnel1,
                      tunnel2,
                      tunnel3,
                      tunnel4,
                      tunnel5,
                      tunnel6,
                      tunnel7,
                      tunnel8,

                      block1,
                      block2,
                      block3,
                      block4,
                      block5,
                      block6,
                      block7,
                      block8,
                      block9,
                      block10,
                      block11,
                      block12,

                      piece1,
                      piece2,
                      piece3,
                      piece4,
                      piece5,
                      piece6,
                      piece7,
                      piece8,
                      piece9,
                      piece10;

    private Image imageFlag,
                  imageCastle;

    private ArrayList<GameObject> gameObjects;
    private ArrayList<GameObject> pieces;

    public ContactCharacterStrategy characterStrategyMario;
    public ContactCharacterStrategy characterStrategyMushroom;
    public ContactCharacterStrategy characterStrategyTurtle;

    public ContactObjectStrategy objectStrategyMario;
    public ContactObjectStrategy objectStrategyMushroom;
    public ContactObjectStrategy objectStrategyTurtle;

    public Platform() {
        super();
        this.background1PositionX = Resources.POSITION_X_BACKGROUND1;
        this.background2PositionX = Resources.POSITION_X_BACKGROUND2;
        this.movement = Resources.MOVEMENT;
        this.xPosition = Resources.X_POSITION;
        this.floorOffsetY = Resources.FLOOR_OFFSET_Y;
        this.heightLimit = Resources.HEIGHT_LIMIT;
        this.imageBackground1 = Utils.getImage(Resources.IMG_BACKGROUND);
        this.imageBackground2 = Utils.getImage(Resources.IMG_BACKGROUND);
        this.castle = Utils.getImage(Resources.IMG_CASTLE);
        this.start = Utils.getImage(Resources.START_ICON);
        this.imageGameOver = Utils.getImage(Resources.IMG_GAME_OVER);

        CharacterFactory factoryCharacter = new CharacterFactoryImplementation();
        ObjectFactory factoryObject = new ObjectFactoryImplementation();

        this.characterStrategyMario = new ContactCharacterMario();
        this.characterStrategyMushroom = new ContactCharacterNotMario();
        this.characterStrategyTurtle = new ContactCharacterNotMario();

        this.objectStrategyMario = new ContactObjectMario();
        this.objectStrategyMushroom = new ContactObjectNotMario();
        this.objectStrategyTurtle = new ContactObjectNotMario();

        mario = factoryCharacter.createMario(300,245);
        mushroom = factoryCharacter.createMushroom(800, 263);
        turtle = factoryCharacter.createTurtle(950, 243);

        tunnel1 = factoryObject.createTunnel(600, 230);
        tunnel2 = factoryObject.createTunnel(1000, 230);
        tunnel3 = factoryObject.createTunnel(1600, 230);
        tunnel4 = factoryObject.createTunnel(1900, 230);
        tunnel5 = factoryObject.createTunnel(2500, 230);
        tunnel6 = factoryObject.createTunnel(3000, 230);
        tunnel7 = factoryObject.createTunnel(3800, 230);
        tunnel8 = factoryObject.createTunnel(4500, 230);

        block1 = factoryObject.createBlock(400, 180);
        block2 = factoryObject.createBlock(1200, 180);
        block3 = factoryObject.createBlock(1270, 170);
        block4 = factoryObject.createBlock(1340, 160);
        block5 = factoryObject.createBlock(2000, 180);
        block6 = factoryObject.createBlock(2600, 160);
        block7 = factoryObject.createBlock(2650, 180);
        block8 = factoryObject.createBlock(3500, 160);
        block9 = factoryObject.createBlock(3550, 140);
        block10 = factoryObject.createBlock(4000, 170);
        block11 = factoryObject.createBlock(4200, 200);
        block12 = factoryObject.createBlock(4300, 210);

        piece1 = factoryObject.createPiece(402, 145);
        piece2 = factoryObject.createPiece(1202, 140);
        piece3 = factoryObject.createPiece(1272, 95);
        piece4 = factoryObject.createPiece(1342, 40);
        piece5 = factoryObject.createPiece(1650, 145);
        piece6 = factoryObject.createPiece(2650, 145);
        piece7 = factoryObject.createPiece(3000, 135);
        piece8 = factoryObject.createPiece(3400, 125);
        piece9 = factoryObject.createPiece(4200, 145);
        piece10 = factoryObject.createPiece(4600, 40);

        this.imageCastle = Utils.getImage(Resources.IMG_CASTLE_FINAL);
        this.imageFlag = Utils.getImage(Resources.IMG_FLAG);

        gameObjects = new ArrayList<GameObject>();

        this.gameObjects.add(tunnel1);
        this.gameObjects.add(tunnel2);
        this.gameObjects.add(tunnel3);
        this.gameObjects.add(tunnel4);
        this.gameObjects.add(tunnel5);
        this.gameObjects.add(tunnel6);
        this.gameObjects.add(tunnel7);
        this.gameObjects.add(tunnel8);

        this.gameObjects.add(block1);
        this.gameObjects.add(block2);
        this.gameObjects.add(block3);
        this.gameObjects.add(block4);
        this.gameObjects.add(block5);
        this.gameObjects.add(block6);
        this.gameObjects.add(block7);
        this.gameObjects.add(block8);
        this.gameObjects.add(block9);
        this.gameObjects.add(block10);
        this.gameObjects.add(block11);
        this.gameObjects.add(block12);

        pieces = new ArrayList<GameObject>();
        this.pieces.add(this.piece1);
        this.pieces.add(this.piece2);
        this.pieces.add(this.piece3);
        this.pieces.add(this.piece4);
        this.pieces.add(this.piece5);
        this.pieces.add(this.piece6);
        this.pieces.add(this.piece7);
        this.pieces.add(this.piece8);
        this.pieces.add(this.piece9);
        this.pieces.add(this.piece10);

        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMovement() {
        return movement;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setBackground2PosX(int background2PosX) {
        this.background2PositionX = background2PosX;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public void setMovement(int mov) {
        this.movement = mov;
    }

    public void setBackground1PositionX(int x) {
        this.background1PositionX = x;
    }

    public void updateBackgroundOnMovement() {
        if (this.xPosition >= 0 && this.xPosition <= 4600) {
            this.xPosition = this.xPosition + this.movement;
            // Moving the screen to give the impression that Mario is walking
            this.background1PositionX = this.background1PositionX - this.movement;
            this.background2PositionX = this.background2PositionX - this.movement;
        }

        // Flipping between background1 and background2
        if (this.background1PositionX == -800) {
            this.background1PositionX = 800;
        }
        else if (this.background2PositionX == -800) {
            this.background2PositionX = 800;
        }
        else if (this.background1PositionX == 800) {
            this.background1PositionX = -800;
        }
        else if (this.background2PositionX == 800) {
            this.background2PositionX = -800;
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = (Graphics2D) g;

        for (int i = 0; i < gameObjects.size(); i++) {
            if (this.mario.isNearby(this.gameObjects.get(i)))
                this.objectStrategyMario.contact(this.mario, this.gameObjects.get(i));

            if (this.mushroom.isNearby(this.gameObjects.get(i)))
                this.objectStrategyMushroom.contact(this.mushroom, this.gameObjects.get(i));

            if (this.turtle.isNearby(this.gameObjects.get(i)))
                this.objectStrategyTurtle.contact(this.turtle, this.gameObjects.get(i));
        }

        for (int i = 0; i < pieces.size(); i++) {
            if (this.mario.contactPiece(this.pieces.get(i), "Piece")) {
                Audio.playSound(Resources.AUDIO_MONEY);
                this.pieces.remove(i);
            }
        }

        if (this.mushroom.isNearby(turtle)) {
            this.characterStrategyMushroom.contact(this.mushroom, turtle);
        }
        if (this.turtle.isNearby(mushroom)) {
            this.characterStrategyTurtle.contact(this.turtle, mushroom);
        }
        if (this.mario.isNearby(mushroom)) {
            this.characterStrategyMario.contact(this.mario, mushroom);
        }
        if (this.mario.isNearby(turtle)) {
            this.characterStrategyMario.contact(this.mario, turtle);
        }

        // Moving fixed objects
        this.updateBackgroundOnMovement();
        if (this.xPosition >= 0 && this.xPosition <= 4600) {
            for (int i = 0; i < gameObjects.size(); i++) {
                gameObjects.get(i).move();
            }

            for (int i = 0; i < pieces.size(); i++) {
                this.pieces.get(i).move();
            }

            this.mushroom.move();
            this.turtle.move();
        }

        g2.drawImage(this.imageBackground1, this.background1PositionX, 0, null);
        g2.drawImage(this.imageBackground2, this.background2PositionX, 0, null);
        g2.drawImage(this.castle, 10 - this.xPosition, 95, null);
        g2.drawImage(this.start, 220 - this.xPosition, 234, null);

        for (int i = 0; i < gameObjects.size(); i++) {
            g2.drawImage(this.gameObjects.get(i).getImageObject(), this.gameObjects.get(i).getCoordinateX(),
                    this.gameObjects.get(i).getCoordinateY(), null);
        }

        for (int i = 0; i < pieces.size(); i++) {
            g2.drawImage(this.pieces.get(i).imageOnMovement(), this.pieces.get(i).getCoordinateX(),
                    this.pieces.get(i).getCoordinateY(), null);
        }

        g2.drawImage(this.imageFlag, Resources.FLAG_X_POS - this.xPosition, Resources.FLAG_Y_POS, null);
        g2.drawImage(this.imageCastle, Resources.CASTLE_X_POS - this.xPosition, Resources.CASTLE_Y_POS, null);

        if (this.mario.isJumping())
            g2.drawImage(this.mario.doJump(), this.mario.getCoordinateX(), this.mario.getCoordinateY(), null);
        else
            g2.drawImage(this.mario.imageOfWalkingThings(Resources.IMGP_CHARACTER_MARIO, Resources.MARIO_FREQUENCY), this.mario.getCoordinateX(), this.mario.getCoordinateY(), null);

        if (this.mushroom.isAlive())
            g2.drawImage(this.mushroom.imageOfWalkingThings(Resources.IMGP_CHARACTER_MUSHROOM, Resources.MUSHROOM_FREQUENCY), this.mushroom.getCoordinateX(), this.mushroom.getCoordinateY(), null);
        else
            g2.drawImage(this.mushroom.deadImage(), this.mushroom.getCoordinateX(), this.mushroom.getCoordinateY() + Resources.MUSHROOM_DEAD_OFFSET_Y, null);

        if (this.turtle.isAlive())
            g2.drawImage(this.turtle.imageOfWalkingThings(Resources.IMGP_CHARACTER_TURTLE, Resources.TURTLE_FREQUENCY), this.turtle.getCoordinateX(), this.turtle.getCoordinateY(), null);
        else
            g2.drawImage(this.turtle.deadImage(), this.turtle.getCoordinateX(), this.turtle.getCoordinateY() + Resources.TURTLE_DEAD_OFFSET_Y, null);
        if(!this.mario.isAlive()) {
            g2.drawImage(imageGameOver, Resources.GAME_OVER_OFFSET_X, Resources.GAME_OVER_OFFSET_Y, null);
        }
    }
}
