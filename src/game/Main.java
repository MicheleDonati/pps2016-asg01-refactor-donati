package game;

import utils.Resources;

import javax.swing.JFrame;

public class Main {

    public static Platform scene;

    public static void main(String[] args) {
        JFrame gameWindow = new JFrame(Resources.WINDOW_TITLE);
        gameWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gameWindow.setSize(Resources.WINDOW_WIDTH, Resources.WINDOW_HEIGHT);
        gameWindow.setLocationRelativeTo(null);
        gameWindow.setResizable(true);
        gameWindow.setAlwaysOnTop(true);

        scene = new Platform();
        gameWindow.setContentPane(scene);
        gameWindow.setVisible(true);

        Thread timer = new Thread(new Refresh());
        timer.start();
    }

}
