package game;

import utils.Resources;
import utils.Utils;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.JPanel;

public class Scene extends JPanel {

    private Image imageBackground,
                  imageMario;

    public Scene() {
        super();
        this.imageBackground = Utils.getImage(Resources.IMG_BACKGROUND);
        this.imageMario = Utils.getImage(Resources.IMG_MARIO_DEFAULT);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = (Graphics2D) g;

        g2.drawImage(this.imageBackground, Resources.BACKGROUND_OFFSET_X, Resources.BACKGROUND_OFFSET_Y, null);
        g2.drawImage(imageMario, Resources.MARIO_OFFSET_X, Resources.MARIO_OFFSET_Y, null);
    }
}
