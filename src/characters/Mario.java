package characters;

import java.awt.Image;

import game.Main;
import objects.GameObject;
import objects.Piece;
import strategies.ContactCharacterStrategy;
import utils.Resources;
import utils.Utils;

public class Mario extends BasicGameCharacter {

    private Image imageMario;

    private boolean jumping;

    private int jumpingExtent;

    public Mario(int x, int y) {
        super(x, y, Resources.WIDTH, Resources.HEIGHT);
        this.imageMario = Utils.getImage(Resources.IMG_MARIO_DEFAULT);
        this.jumping = false;
        this.jumpingExtent = Resources.EXTENSION_OF_JUMP;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public Image doJump() {
        String stringToDefineJumpImageInGame;

        this.jumpingExtent++;

        if (this.jumpingExtent < Resources.JUMPING_LIMIT) {
            if (this.getCoordinateY() > Main.scene.getHeightLimit())
                this.setCoordinateY(this.getCoordinateY() - 4);
            else this.jumpingExtent = Resources.JUMPING_LIMIT;

            stringToDefineJumpImageInGame = this.isToRight() ? Resources.IMG_MARIO_SUPER_DX : Resources.IMG_MARIO_SUPER_SX;
        } else if (this.getCoordinateY() + this.getHeight() < Main.scene.getFloorOffsetY()) {
            this.setCoordinateY(this.getCoordinateY() + 1);
            stringToDefineJumpImageInGame = this.isToRight() ? Resources.IMG_MARIO_SUPER_DX : Resources.IMG_MARIO_SUPER_SX;
        } else {
            stringToDefineJumpImageInGame = this.isToRight() ? Resources.IMG_MARIO_ACTIVE_DX : Resources.IMG_MARIO_ACTIVE_SX;
            this.jumping = false;
            this.jumpingExtent = 0;
        }

        return Utils.getImage(stringToDefineJumpImageInGame);
    }

    public boolean contactPiece(GameObject piece, String isAPiece) {
        if (this.hitBack(piece) || this.hitAbove(piece) || this.hitAhead(piece)
                || this.hitBelow(piece))
            return true;

        return false;
    }

    public Image deadImage(){
        return null;
    }

    @Override
    public void setOffsetX(int x) {

    }
}
