package characters;

import java.awt.Image;

import factory.CharacterFactory;
import objects.GameObject;
import objects.Piece;
import strategies.ContactCharacterStrategy;
import utils.Resources;
import utils.Utils;

public class Mushroom extends BasicGameCharacter implements Runnable {

    private Image mushroomImage;

    private final int PAUSE = 15;

    private int offsetX;

    public Mushroom(int x, int y) {
        super(x, y, Resources.MUSHROOM_WIDTH, Resources.MUSHROOM_HEIGHT);
        this.setToRight(true);
        this.setMoving(true);
        this.offsetX = Resources.MUSHROOM_OFFSET_COORDINATE_X;
        this.mushroomImage = Utils.getImage(Resources.IMG_MUSHROOM_DEFAULT);

        Thread fewSecondsMushroom = new Thread(this);
        fewSecondsMushroom.start();
    }

    //getters
    public Image getMushroomImage() {
        return mushroomImage;
    }

    public void moveMushroom() {
        this.offsetX = isToRight() ? 1 : -1;
        this.setCoordinateX(this.getCoordinateX() + this.offsetX);
    }

    @Override
    public void run() {
        while (true) {
            if (this.alive == true) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    @Override
    public boolean isJumping() {
        return false;
    }

    @Override
    public void setJumping(boolean jumping) {
    }

    @Override
    public Image doJump() {
        return null;
    }

    @Override
    public boolean contactPiece(GameObject piece, String isAPiece) {
        return false;
    }


    public Image deadImage() {
        return Utils.getImage(this.isToRight() ? Resources.IMG_MUSHROOM_DEAD_DX : Resources.IMG_MUSHROOM_DEAD_SX);
    }

    public void setOffsetX(int x){
        this.offsetX = x;
    }
}
