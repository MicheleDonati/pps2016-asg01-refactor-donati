package characters;

import java.awt.Image;

public interface GameCharacter {

	Image imageOfWalkingThings(String name, int frequency);

}
