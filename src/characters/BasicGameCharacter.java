package characters;

import java.awt.Image;

import game.Main;
import objects.GameObject;
import strategies.ContactCharacterStrategy;
import utils.Resources;
import utils.Utils;

public abstract class BasicGameCharacter implements GameCharacter {

    private int width,
                height,
                coordinateX,
                coordinateY;

    protected boolean moving,
                      toRight,
                      alive;

    public int counter;

    public ContactCharacterStrategy characterStrategy;


    public BasicGameCharacter(int x, int y, int width, int height/*, ContactCharacterStrategy characterStrategy*/) {
        this.coordinateX = x;
        this.coordinateY = y;
        this.height = height;
        this.width = width;
        this.counter = Resources.STARTER_COUNTER;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
       // this.characterStrategy = characterStrategy;
    }

    public abstract boolean isJumping();

    public abstract void setJumping(boolean jumping);

    public abstract Image doJump();

    public abstract boolean contactPiece(GameObject piece, String isAPiece);

   /* public void contact(BasicGameCharacter characterItself, BasicGameCharacter character){
        this.characterStrategy.contact(characterItself, character);
    }*/

    public abstract Image deadImage();

    public abstract void setOffsetX(int x);

    public int getCoordinateX() {
        return coordinateX;
    }

    public int getCoordinateY() {
        return coordinateY;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getCounter() {
        return counter;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isMoving() {
        return moving;
    }

    public boolean isToRight() {
        return toRight;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setCoordinateX(int x) {
        this.coordinateX = x;
    }

    public void setCoordinateY(int y) {
        this.coordinateY = y;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Image imageOfWalkingThings(String name, int frequency) {
        String stringForChooseInGameImage = Resources.IMG_BASE + name + (!this.moving || ++this.counter % frequency == 0 ? Resources.IMGP_STATUS_ACTIVE : Resources.IMGP_STATUS_NORMAL) +
                (this.toRight ? Resources.IMGP_DIRECTION_DX : Resources.IMGP_DIRECTION_SX) + Resources.IMG_EXT;
        return Utils.getImage(stringForChooseInGameImage);
    }

    public void move() {
        if (Main.scene.getxPosition() >= 0) {
            this.coordinateX = this.coordinateX - Main.scene.getMovement();
        }
    }

    public boolean hitAhead(GameObject object) {
        if (this.coordinateX + this.width < object.getCoordinateX() || this.coordinateX + this.width > object.getCoordinateX() + 5 ||
                this.coordinateY + this.height <= object.getCoordinateY() || this.coordinateY >= object.getCoordinateY() + object.getHeight()) {
            return false;
        } else
            return true;
    }

    public boolean hitBack(GameObject object) {
        if (this.coordinateX > object.getCoordinateX() + object.getWidth() || this.coordinateX + this.width < object.getCoordinateX() + object.getWidth() - 5 ||
                this.coordinateY + this.height <= object.getCoordinateY() || this.coordinateY >= object.getCoordinateY() + object.getHeight()) {
            return false;
        } else
            return true;
    }

    public boolean hitBelow(GameObject object) {
        if (this.coordinateX + this.width < object.getCoordinateX() + 5 || this.coordinateX > object.getCoordinateX() + object.getWidth() - 5 ||
                this.coordinateY + this.height < object.getCoordinateY() || this.coordinateY + this.height > object.getCoordinateY() + 5) {
            return false;
        } else
            return true;
    }

    public boolean hitAbove(GameObject object) {
        if (this.coordinateX + this.width < object.getCoordinateX() + 5 || this.coordinateX > object.getCoordinateX() + object.getWidth() - 5 ||
                this.coordinateY < object.getCoordinateY() + object.getHeight() || this.coordinateY > object.getCoordinateY() + object.getHeight() + 5) {
            return false;
        } else return true;
    }

    public boolean hitAhead(BasicGameCharacter character) {
        if (this.isToRight() == true) {
            if (this.coordinateX + this.width < character.getCoordinateX() || this.coordinateX + this.width > character.getCoordinateX() + 5 ||
                    this.coordinateY + this.height <= character.getCoordinateY() || this.coordinateY >= character.getCoordinateY() + character.getHeight()) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public boolean hitBack(BasicGameCharacter character) {
        if (this.coordinateX > character.getCoordinateX() + character.getWidth() || this.coordinateX + this.width < character.getCoordinateX() + character.getWidth() - 5 ||
                this.coordinateY + this.height <= character.getCoordinateY() || this.coordinateY >= character.getCoordinateY() + character.getHeight())
            return false;
        return true;
    }

    public boolean hitBelow(BasicGameCharacter character) {
        if (this.coordinateX + this.width < character.getCoordinateX() || this.coordinateX > character.getCoordinateX() + character.getWidth() ||
                this.coordinateY + this.height < character.getCoordinateY() || this.coordinateY + this.height > character.getCoordinateY())
            return false;
        return true;
    }

    public boolean isNearby(BasicGameCharacter character) {
        if ((this.coordinateX > character.getCoordinateX() - Resources.PROXIMITY_MARGIN && this.coordinateX < character.getCoordinateX() + character.getWidth() + Resources.PROXIMITY_MARGIN)
                || (this.coordinateX + this.width > character.getCoordinateX() - Resources.PROXIMITY_MARGIN && this.coordinateX + this.width < character.getCoordinateX() + character.getWidth() + Resources.PROXIMITY_MARGIN))
            return true;
        return false;
    }

    public boolean isNearby(GameObject object) {
        if ((this.coordinateX > object.getCoordinateX() - Resources.PROXIMITY_MARGIN && this.coordinateX < object.getCoordinateX() + object.getWidth() + Resources.PROXIMITY_MARGIN) ||
                (this.getCoordinateX() + this.width > object.getCoordinateX() - Resources.PROXIMITY_MARGIN && this.coordinateX + this.width < object.getCoordinateX() + object.getWidth() + Resources.PROXIMITY_MARGIN))
            return true;
        return false;
    }
}
