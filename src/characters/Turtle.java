package characters;

import java.awt.Image;

import objects.GameObject;
import objects.Piece;
import strategies.ContactCharacterStrategy;
import utils.Resources;
import utils.Utils;

public class Turtle extends BasicGameCharacter implements Runnable {

    private Image imageTurtle;

    private final int PAUSE = 15;

    private int dxTurtle;

    public Turtle(int X, int Y) {
        super(X, Y, Resources.TURTLE_WIDTH, Resources.TURTLE_HEIGHT);
        super.setToRight(true);
        super.setMoving(true);
        this.dxTurtle = 1;
        this.imageTurtle = Utils.getImage(Resources.IMG_TURTLE_IDLE);

        Thread chronoTurtle = new Thread(this);
        chronoTurtle.start();
    }

    public Image getTurtleImage() {
        return imageTurtle;
    }

    public void move() {
        this.dxTurtle = isToRight() ? 1 : -1;
        super.setCoordinateX(super.getCoordinateX() + this.dxTurtle);
    }

    @Override
    public void run() {
        while (true) {
            if (this.alive) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public Image deadImage() {
        return Utils.getImage(Resources.IMG_TURTLE_DEAD);
    }

    @Override
    public boolean isJumping() {
        return false;
    }

    @Override
    public void setJumping(boolean jumping) {
    }

    @Override
    public Image doJump() {
        return null;
    }

    @Override
    public boolean contactPiece(GameObject piece, String isAPiece) {
        return false;
    }

    public void setOffsetX(int x){
        this.dxTurtle = x;
    }
}
